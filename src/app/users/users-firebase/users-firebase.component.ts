import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { Router } from "@angular/router"; //Login without JWT

@Component({
  selector: 'users-firebase',
  templateUrl: './users-firebase.component.html',
  styleUrls: ['./users-firebase.component.css']
})
export class UsersFirebaseComponent implements OnInit {

    users;
  
    //only indepense injection
    constructor(private service:UsersService,private router:Router) { }
  
    //רוצים למשוך את הרשימה שהקומפוננט נוצר
    //פונקציה שמתעוררת ברגע שהקומפוננט נוצר לאחר הקונסטרקטור
    ngOnInit() {
      this.service.getUserFire().subscribe(response=>{
        console.log(response);
        this.users = response;
      });
      //Login without JWT
    var value = localStorage.getItem('auth');
    
    /*if(value == 'true'){   
     // this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    }  */
    }
  
  }
  
