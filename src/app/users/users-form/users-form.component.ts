import { UsersService } from './../users.service';
import { Component, OnInit , Output , EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {
 //בניית ערוץ התקשורת בין message form to message
 @Output() addUser:EventEmitter<any> = new EventEmitter<any>(); //any מגדיר סוג מסוים של מידע, במקרה הזה כל סוג
 @Output() addUserPs:EventEmitter<any> = new EventEmitter<any>(); //אירוע פסימי

 service:UsersService;

 //Reactive Form
 usrform = new FormGroup({
   name :new FormControl(),
   phone:new FormControl()
 });

   sendData(){
     this.addUser.emit(this.usrform.value.name);
     console.log(this.usrform.value);//לוקח את הערכים שהכנסנו לטופס ושולח אותם לקונסול
      this.service.postUsers(this.usrform.value).subscribe(
        response =>{        
          console.log(response.json())
          this.addUserPs.emit();
        }
      )
    }
  

 constructor(service: UsersService, private formBuilder:FormBuilder) { 
   this.service = service;
 }

 ngOnInit() {
  this.usrform = this.formBuilder.group({
  name:  [null, [Validators.required]],
  phone: [null, Validators.required],
  });	          
 }

}
