// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: 'http://localhost/slimexampletest/',
  firebase:{
    apiKey: "AIzaSyDKFgW4ub6BpbgIbTqa2BGxbGbB5X4vi0I",
    authDomain: "users-15c0d.firebaseapp.com",
    databaseURL: "https://users-15c0d.firebaseio.com",
    projectId: "users-15c0d",
    storageBucket: "users-15c0d.appspot.com",
    messagingSenderId: "750706666861"
  }
};
